jQuery(document).ready(function ( $ ) {
    $('.color-picker').wpColorPicker();
    
    parent1=$('#_yith-pp-free-char').closest('div');
    parent2=$('#_yith-pp-price').closest('div'); 
    $('#option-fixed').on('click', function(){
        parent2.show();
        parent1.show();
   });
   $('#option-ppp').on('click', function(){
        parent1.show();
        parent2.show();
    });
    $('#option-free').on('click', function(){
    parent2.hide();
    parent1.hide();
    });

    // Selecction enable product purchase.
    $('#_yith-pp-enable').on('click', function(){
        if ( $('#_yith-pp-enable').prop('checked') ) {
            $('._yith-pp-cont-pp-toggle').show();
            $('._yith-pp-cont-pp-text').show();
            $('._yith-pp-cont-pp-text-area').show();
            $('._yith-pp-cont-pp-radio').show();
            $('#_yith-pp-enable').val('yes');
            if ( $('#_yith-pp-show-badge').prop('checked') ) {
                $('._yith-pp-cont-pp-badge-text').show();
                $('._yith-pp-cont-pp-color-picker').show();
            }
        }else{
            $('._yith-pp-cont-pp-toggle').hide();
            $('._yith-pp-cont-pp-text').hide();
            $('._yith-pp-cont-pp-text-area').hide();
            $('._yith-pp-cont-pp-radio').hide();
            $('._yith-pp-cont-pp-number').hide();
            $('._yith-pp-cont-pp-color-picker').hide();   
            $('._yith-pp-cont-pp-badge-text').hide();
            $('#_yith-pp-enable').val('no');     
            $('#_yith-pp-show-badge').val('no');
        }
   });
   // Selecction enable show badge product purchase.
   $('#_yith-pp-show-badge').on('click', function(){
        if ( $('#_yith-pp-show-badge').prop('checked') ) {
            $('._yith-pp-cont-pp-color-picker').show();
            $('._yith-pp-cont-pp-badge-text').show();
            $('#_yith-pp-show-badge').val('yes');
        }else{
            $('._yith-pp-cont-pp-color-picker').hide();
            $('._yith-pp-cont-pp-badge-text').hide();
            $('#_yith-pp-show-badge').val('no');
        }
    });
    // Show previous options.
    if ( $('#_yith-pp-enable').prop('checked') ) {
        $('._yith-pp-cont-pp-toggle').show();
        $('._yith-pp-cont-pp-text').show();
        $('._yith-pp-cont-pp-text-area').show();
        $('._yith-pp-cont-pp-radio').show();
        $('._yith-pp-cont-pp-number').show();
        $('#_yith-pp-enable').val('yes');
        if ( $('#_yith-pp-show-badge').prop('checked') ) {
            $('._yith-pp-cont-pp-badge-text').show();
            $('._yith-pp-cont-pp-color-picker').show();
        }
        if ( $('#option-free').prop('checked') ) {
            $('._yith-pp-cont-pp-number').hide();
        }
    }
    

    //group inputs
    cont=$('._yith-setting-option').closest('tbody');
    cont.css('display','flex');
    cont2=$('._yith-setting-option').closest('td');
    cont2.attr('class','null-table');
    cont3=cont2.siblings('th');
    cont3.attr('class','null-table');
    cont4=$('._yith-setting-option').closest('tr');
    cont4.css('display','grid');
});