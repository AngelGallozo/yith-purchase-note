jQuery( function ( $ ) {
    $("#_yith-pp-note").on('change keyup',function() {  
        var msj = $(this).val();
        console.log('entro');
        var post_id =$(this).attr('data-post_id');
        $.ajax({
            type: "POST",
            dataType: "json",
            url: 'wp-admin/admin-ajax.php',
            data: {action: 'increment_price', msj:msj.length, post_id: post_id},
            error: function(response) {
                console.log(response);
            },
            success: function(response) {
                 $('#_yith-pp-cont-note-price').text(response['message']);
            }
        });
    });
});