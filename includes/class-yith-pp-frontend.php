<?php // phpcs:ignore
/**
 * This file belongs to the YITH PP Plugin Product Purchase Note.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

if ( ! defined( 'YITH_PP_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PP_Frontend' ) ) {
	/**
	 * YITH_PP_Frontend
	 */
	class YITH_PP_Frontend {
		/**
		 * Main Instance
		 *
		 * @var YITH_PT_Frontend
		 * @since 1.0.0
		 * @access private
		 */
		private static $instance;
		/**
		 * Main plugin Instance
		 * @return YITH_PP_Frontend Main instance
		 * @author Angel Gallozo <luisangelgallozo@gmail.com>
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}
		/**
		 * YITH_PP_Frotend constructor.
		 */
		private function __construct() {
			// Add Note in Product Page.
			add_action( 'woocommerce_before_add_to_cart_button', array( $this, 'pp_add_note_prod_page' ) );
			// Save Note Data.
			add_filter( 'woocommerce_add_cart_item_data', array( $this, 'pp_save_note_data' ), 10, 2 );
			// Add Note in Cart.
			add_filter( 'woocommerce_get_item_data', array( $this, 'pp_add_note_cart_page' ), 10, 2 );
			// Add Note in CheckOut.
			add_action( 'woocommerce_add_order_item_meta', array( $this, 'pp_add_note_order_page' ), 10, 2 );
			// Enqueue Scripts.
			add_action( 'wp_enqueue_scripts', array( $this, 'pp_enqueue_scripts' ) );
			// Add Badge in Shop.
			add_action( 'woocommerce_before_shop_loop_item', array( $this, 'pp_add_badge_shop' ) );
			// Add Badge in Product.
			add_action( 'woocommerce_product_thumbnails', array( $this, 'pp_add_badge_product' ) );
			// Update price Note.
			add_action( 'woocommerce_before_calculate_totals', array( $this, 'pp_update_price_note' ), 10, 2 );
		}
		/**
		 * Enqueue_scripts Function
		 *
		 * @return void
		 */
		public function pp_enqueue_scripts() {
			wp_register_style( 'yith-pp-frontend-css', YITH_PP_DIR_ASSETS_CSS_URL . '/frontend-style.css', array(), YITH_PP_VERSION );
			wp_register_script( 'yith-pp-frontend-js', YITH_PP_DIR_ASSETS_JS_URL . '/frontend-main.js', array( 'jquery' ), YITH_PP_VERSION, false );
			wp_enqueue_script( 'yith-pp-frontend-js' );
			wp_enqueue_style( 'yith-pp-frontend-css' );
			$this->pp_set_styles_badge_note();
		}
		/**
		 * Add badge in Product Shop
		 *
		 * @return void
		 */
		public function pp_add_badge_shop() {
			global $post;
			if ( 'product' === $post->post_type && 'yes' === get_post_meta( $post->ID, '_yith_pp_show_badge', true ) ) {
				echo '<span class="_yith-pp-badge-shop" style="background:' . esc_attr( get_post_meta( $post->ID, '_yith_pp_badge_bg_color', true ) ) .'; color:' . esc_attr( get_post_meta( $post->ID, '_yith_pp_badge_text_color', true ) ) . ';">' . esc_html( get_post_meta( $post->ID, '_yith_pp_badge_text', true ) ) . '</span>';
			}
		}
		/**
		 * Add badge in Product
		 *
		 * @return void
		 */
		public function pp_add_badge_product() {
			global $post;
			if ( 'product' === $post->post_type && 'yes' === get_post_meta( $post->ID, '_yith_pp_show_badge', true ) ) {
				echo '<span class="_yith-pp-badge-prod"	style="background:' . esc_attr( get_post_meta( $post->ID, '_yith_pp_badge_bg_color', true ) ) .'; color:'. esc_attr( get_post_meta ( $post->ID, '_yith_pp_badge_text_color', true ) ) . ';">' . esc_html( get_post_meta( $post->ID, '_yith_pp_badge_text', true ) ) . '</span>';
			}
		}
		/**
		 * Add Note in Product Page
		 *
		 * @return void
		 */
		public function pp_add_note_prod_page() {
			global $post;
			$product    = wc_get_product( $post->ID );
			$enable     = $product->get_meta( '_yith_pp_enable' );
			$type_note  = $product->get_meta( '_yith_pp_field_type' );
			$free_chars = $product->get_meta ( '_yith_pp_free_char' );
			if ( isset( $enable ) && 'yes' === $enable && isset( $type_note ) ) {
				$lb_note    = $product->get_meta( '_yith_pp_note_label' );
				$desc_note  = $product->get_meta( '_yith_pp_note_desc' );
				$value_note = $product->get_meta( '_yith_pp_note' );
				$fields = array(
					'type'      => $type_note,
					'label'     => $lb_note,
					'desc'      => $desc_note,
					'id'        => '_yith-pp-note',
					'name'      => '_yith_pp_note',
					'value'     => ( ( '' !== $value_note ) ? $value_note : '' ),
					'enable'    => 'yes',
					'post_id'   => $post->ID,
					'container' => '',
				);
				echo '<div class="_yith-cont-note-prod">';
				yith_pp_get_view( '/input-fields/' . $fields['type'] . '.php', $fields );
				if ( 'free' !== $product->get_meta( '_yith_pp_price_settings' ) ) {
					echo '<span id="_yith-pp-cont-note-about-price">' . esc_html__( 'Price: ', 'yith-plugin-product-purchase' ) . ' ' 
					. esc_attr( $product->get_meta( '_yith_pp_price' ) ) . '€ (' 
					. esc_html( $free_chars ) . esc_html__( ' free characteres ', 'yith-plugin-product-purchase' ) . ') </span> <span id="_yith-pp-cont-note-price"> + 0€ </>';
				}
				echo '</div>';
			}
		}		
		/**
		 * Save Note data
		 *
		 * @return void
		 */
		public function pp_save_note_data( $cart_item, $product_id ) {
			$product = wc_get_product( $product_id );
			if ( isset( $_POST['_yith_pp_note'] ) ) {
				$note = $_POST['_yith_pp_note'];
				$cart_item['_yith_pp_note'] = sanitize_text_field( $_POST['_yith_pp_note'] );

				// Calculate Price.
				if ( null !== $product->get_meta( '_yith_pp_enable' ) && 'yes' === $product->get_meta( '_yith_pp_enable' ) ) {
					$price      = 0;
					$price_pp   = $product->get_meta( '_yith_pp_price' );
					$option     = $product->get_meta( '_yith_pp_price_settings' );
					$chars_free = intval( $product->get_meta( '_yith_pp_free_char' ) );
					if ( strlen( $note ) > $chars_free ) {
						switch ( $option ) {
							case 'fixed':
								$price = $price_pp;
								break;
							case 'price-per-char':
								$price = ( strlen( $note ) - $chars_free ) * $price_pp;
								break;
						}
					}
				}
				// Save Price.
				$cart_item['_yith_pp_note_price'] = $price;
			}
			return $cart_item;
		}
		/**
		 * Add Note in Cart Page
		 *
		 * @return void
		 */
		public function pp_add_note_cart_page( $data, $cart_item ) {
			if ( isset( $cart_item['_yith_pp_note'] ) ) {
				$data[] = array(
					'name' => __( 'Product Purchase Note', 'yith-plugin-product-purchase' ),
					'value' => sanitize_text_field( $cart_item['_yith_pp_note'] ),
				);
			}
			return $data;
		}
		/**
		 * Add Note Order Page
		 *
		 * @param  mixed $item_id
		 * @param  mixed $values
		 * @return void
		 */
		public function pp_add_note_order_page( $item_id, $values ) {
			if ( ! empty( $values['_yith_pp_note'] ) ) {
				wc_add_order_item_meta( $item_id, __( 'Product Purchase Note', 'yith-plugin-product-purchase' ), $values['_yith_pp_note'], true );
			}
		}	
		/**
		 * Set Styles Badge & Note Box
		 *
		 * @return void
		 */
		public function pp_set_styles_badge_note() {
			wp_enqueue_style( 'yith-pp-frontend-css' );
			// Badge Position in Shop.
			$shop_position = get_option( '_yith_pp_bdg_pos_shop', 'top_right' );
			if ( 'top_left' === $shop_position ) {
				$custom_css = "
					._yith-pp-badge-shop{
						right:auto;
						left: 0;}";
				wp_add_inline_style( 'yith-pp-frontend-css', $custom_css );
			}
			// Badge Position in Product.
			$prod_position = get_option( '_yith_pp_bdg_pos_prod', 'top_right' );
			if ( 'top_left' === $prod_position ) {
				$custom_css = "
					._yith-pp-badge-prod{
						right:auto;
						left: 0;}";
				wp_add_inline_style( 'yith-pp-frontend-css', $custom_css );
			}
			// Set note box style.
			$note_pad_top       = get_option( '_yith_pp_set_padding_top', 20 );
			$note_pad_right     = get_option( '_yith_pp_set_padding_right', 25 );
			$note_pad_left      = get_option( '_yith_pp_set_padding_left', 25 );
			$note_pad_bottom    = get_option( '_yith_pp_set_padding_bottom', 25 );
			$note_border_width  = get_option( '_yith_pp_set_border_width', 1 );
			$note_border_style  = get_option( '_yith_pp_set_border_style', 'solid' );
			$note_border_color  = get_option( '_yith_pp_set_border_color', '#d8d8d8' );
			$note_border_radius = get_option( '_yith_pp_set_border_radius', 7 );
			$custom_css = "
				._yith-cont-note-prod{
					padding: {$note_pad_top}px {$note_pad_right }px {$note_pad_bottom}px {$note_pad_left}px;
					border-radius: {$note_border_radius}px;
					border-style: {$note_border_style};
					border-color: {$note_border_color};
					border-width: {$note_border_width}px;
				}";
				wp_add_inline_style( 'yith-pp-frontend-css', $custom_css );
		}
		/**
		 * Update Price Note
		 *
		 * @return void
		 */
		public function pp_update_price_note( $cart ) {
			foreach ( $cart->get_cart() as $cart_item => $item ) {
				if ( isset( $item['_yith_pp_note'] ) ) {
					$price       = $item['data']->price;
					$total_price = $price + $item['_yith_pp_note_price'];
					if ( $price !== $total_price ) {
						$item['data']->set_price( $total_price );
					}
				}
			}
		}
	}
}
