<?php
/**
 * This file belongs to the YITH PP Plugin Personalize Products
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

if ( ! defined( 'YITH_PP_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PP_Plugin_P_Products' ) ) {	
	/**
	 * YITH_PP_Plugin
	 */
	class YITH_PP_Plugin_P_Products {
        /**
		 * Main Instance
		 *
		 * @var YITH_PP_Plugin_P_Products
		 * @since 1.0.0
		 * @access private
		 */

		private static $instance;
        /**
		 * Main Admin Instance
		 *
		 * @var YITH_PP_Plugin_P_Products_Admin
		 * @since 1.0.0
		 */
		public $admin = null;
		/**
		 * Main Frontpage Instance
		 *
		 * @var YITH_PP_Plugin_P_Products_Frontend
		 * @since 1.0.0
		 */
        public $frontend = null;
        /**
         * Main plugin Instance
         *
         * @return YITH_PP_Plugin_P_Products Main instance
         * @author Angel Gallozo <luisangelgallozo@gmail.com>
         */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
        }
        
		/**
		 * YITH_PP_Plugin_P_Products constructor.
		 */
		private function __construct() {
            $require = apply_filters( 'yith_pp_require_class',
				 array(
					'common'   => array(
						'includes/functions.php',
					),
					'admin' => array(
						'includes/class-yith-pp-admin.php',
					),
					'frontend' => array(
						'includes/class-yith-pp-frontend.php',
					),
				)
			);
			$this->_require($require);
			$this->init_classes();
			// Finally call the init function
			$this->init();
		}
	
		/**
		 * Add the main classes file
		 *
		 * Include the admin and frontend classes
		 *
		 * @param $main_classes array The require classes file path
		 *
		 * @author Angel Gallozo <luisangelgallozo@gmail.com>
		 * @since  1.0.0
		 *
		 * @return void
		 * @access protected
		 */
		protected function _require($main_classes)
		{
			foreach ( $main_classes as $section => $classes ) {
				foreach ( $classes as $class ) {
					if ( 'common' == $section || ( 'frontend' == $section && !is_admin() || (defined( 'DOING_AJAX' ) && DOING_AJAX ) ) || ( 'admin' == $section && is_admin() ) && file_exists( YITH_PP_DIR_PATH . $class ) ) {
						require_once( YITH_PP_DIR_PATH . $class );
					}
				}
			}
		}

		/**
		 * Init common class if they are necessary
		 * @author Angel Gallozo <luisangelgallozo@gmail.com>
		 * @since  1.0.0
		 **/
		public function init_classes(){
		}

		/**
         * Function init()
         *
         * Instance the admin or frontend classes
         *
         * @author Angel Gallozo <luisangelgallozo@gmail.com>
         * @since  1.0.0
         * @return void
         * @access protected
         */
        public function init()
        {
            if ( is_admin() ) {
                $this->admin =  YITH_PP_Admin::get_instance();
            }

            if ( !is_admin() || (defined('DOING_AJAX') && DOING_AJAX)) {
                $this->frontend = YITH_PP_Frontend::get_instance();
            }
        }

	}	
}
/**
 * Get the YITH_PP_Plugin_Testimonial instance
 *
 * @return YITH_PP_Plugin_P_Products
 */
if ( ! function_exists( 'yith_pp_plugin_p_products' ) ) {
	function yith_pp_plugin_p_products() {
		return YITH_PP_Plugin_P_Products::get_instance();
	}
}