<?php //phpcs:ignore
/**
 * This file belongs to the YITH PP Plugin Product Purchase Note.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

if ( ! defined( 'YITH_PP_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PP_Admin' ) ) {
	/**
	 * YITH_PP_Admin
	 */
	class YITH_PP_Admin {
		/**
		 * Main Instance
		 * @var YITH_PP_Admin
		 * @since 1.0.0
		 * @access private
		 */
		private static $instance;
		/**
		 * Main plugin Instance
		 * @return YITH_PP_Admin Main instance
		 * @author Angel Gallozo <luisangelgallozo@gmail.com>
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}
		/**
		 * YITH_PP_Admin constructor.
		 */
		private function __construct() {
			// Add js for the shortcode.
			add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_scripts' ) );
		}
		/**
		 * Enqueue_scripts
		 *
		 * @return void
		 */
		public function enqueue_scripts() {
			wp_register_style( 'yith-pp-admin-css', YITH_PP_DIR_ASSETS_CSS_URL . '/admin-style.css', array(), YITH_PP_VERSION );
			wp_enqueue_script( 'yith-pp-admin-js', YITH_PP_DIR_ASSETS_JS_URL . '/admin-main.js', array( 'jquery' ), YITH_PP_VERSION, true );
			wp_enqueue_style( 'wp-color-picker' );
			wp_enqueue_style( 'yith-pp-admin-css' );
			wp_enqueue_script( 'my-script-handle', YITH_PP_DIR_ASSETS_JS_URL . '/admin-main.js', array( 'wp-color-picker' ), YITH_PP_VERSION, true );
		}
	}
}
