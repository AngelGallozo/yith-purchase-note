<?php //phpcs:ignore
/**
 * This file belongs to the YITH PP Plugin Product Purchase Note.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

if ( ! defined( 'YITH_PP_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PP_SO' ) ) {
	/**
	 * YITH_PP_SO
	 */
	class YITH_PP_SO {
		/**
		 * Main Instance
		 *
		 * @var YITH_PP_SO
		 * @since 1.0.0
		 * @access private
		 */
		private static $instance;
		/**
		 * Main plugin Instance
		 * @return YITH_PP_SO Main instance
		 * @author Angel Gallozo <luisangelgallozo@gmail.com>
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}
		/**
		 * YITH_PP_SO constructor.
		 */
		private function __construct() {
			// Admin menu.
			add_action( 'admin_menu', array( $this, 'wc_pp_create_settings') );
			add_action( 'admin_init', array( $this, 'wc_pp_register_settings' ) );
		}
		/**
		 *  Create menu for general options
		 */
		public function wc_pp_create_settings() {
			add_menu_page(
				esc_html__( 'Plugin PP Options', 'yith-plugin-product-purchase' ),
				esc_html__( 'Plugin PP Options', 'yith-plugin-product-purchase' ),
				'manage_options',
				'plugin_testimonial_options',
				array( $this, 'pp_custom_menu_page' ),
				'',
				40
			); 
		}
		/**
		 *  Callback custom menu page
		 */
		public function pp_custom_menu_page() {
			yith_pp_get_view( '/admin/plugin-options-panel.php', array() );
		}
		/**
		 * Add the fields in the shortcode attribute management page
		 */
		public function wc_pp_register_settings() {
			$page_name      = 'pp-options-page';
			$setting_fields = array(
				array(
					'title'        => __( 'Padding', 'yith-plugin-product-purchase' ),
					'id'           => 'yith-pp-section-padding',
					'section_name' => 'options_padding',
					'container'    => '_yith-setting-option',
					'fields'       => array(
						array(
							'type'      => 'number',
							'label'     => __( 'top', 'yith-plugin-product-purchase' ),
							'id'        => '_yith-pp-set-padding-top',
							'name'      => '_yith_pp_set_padding_top',
							'value'     => intval( get_option( '_yith_pp_set_padding_top', 20 ) ),
							'container' => '_yith-setting-option',
							'enable'    => 'yes',
							'adm_pro'   => 'no',
						),
						array(
							'type'      => 'number',
							'label'     => __( 'right', 'yith-plugin-product-purchase' ),
							'id'        => '_yith-pp-set-padding-right',
							'name'      => '_yith_pp_set_padding_right',
							'value'     => intval( get_option( '_yith_pp_set_padding_right', 25 ) ),
							'container' => '_yith-setting-option',
							'enable'    => 'yes',
							'adm_pro'   => 'no',
						),
						array(
							'type'      => 'number',
							'label'     => __( 'bottom', 'yith-plugin-product-purchase' ),
							'id'        => '_yith-pp-set-padding-bottom',
							'name'      => '_yith_pp_set_padding_bottom',
							'value'     => intval( get_option( '_yith_pp_set_padding_bottom', 25 ) ),
							'container' => '_yith-setting-option',
							'enable'    => 'yes',
							'adm_pro'   => 'no',
						),
						array(
							'type'      => 'number',
							'label'     => __( 'left', 'yith-plugin-product-purchase' ),
							'id'        => '_yith-pp-set-padding-left',
							'name'      => '_yith_pp_set_padding_left',
							'value'     => intval( get_option( '_yith_pp_set_padding_left', 25 ) ),
							'container' => '_yith-setting-option',
							'enable'    => 'yes',
							'adm_pro'   => 'no',
						),
					),
				),
				array(
					'title'        => __( 'Border', 'yith-plugin-product-purchase' ),
					'id'           => 'yith-pp-section-border',
					'section_name' => 'options_border',
					'container'    => '_yith-setting-option',
					'fields'       => array(
						array(
							'type'      => 'number',
							'label'     => __( 'width', 'yith-plugin-product-purchase' ),
							'id'        => '_yith-pp-set-border-width',
							'name'      => '_yith_pp_set_border_width',
							'value'     => intval( get_option( '_yith_pp_set_border_width', 1 ) ),
							'container' => '_yith-setting-option',
							'enable'    => 'yes',
							'adm_pro'   => 'no',
						),
						array(
							'type'      => 'select',
							'label'     => __( 'style', 'yith-plugin-product-purchase' ),
							'id'        => '_yith-pp-set-border-style',
							'name'      => '_yith_pp_set_border_style',
							'value'     => get_option( '_yith_pp_set_border_style', 'solid' ),
							'container' => '_yith-setting-option',
							'options'   => array(
								array(
									'value' => 'none',
									'label' => __( 'None', 'yith-plugin-product-purchase' ),
								),
								array(
									'value' => 'solid',
									'label' => __( 'Solid', 'yith-plugin-product-purchase' ),
								),
								array(
									'value' => 'dashed',
									'label' => __( 'Dashed', 'yith-plugin-product-purchase' ),
								),
								array(
									'value' => 'dotted',
									'label' => __( 'Dotted', 'yith-plugin-product-purchase' ),
								),
								array(
									'value' => 'unset',
									'label' => __( 'Unset', 'yith-plugin-product-purchase' ),
								),
								array(
									'value' => 'inset',
									'label' => __( 'Inset', 'yith-plugin-product-purchase' ),
								),
								array(
									'value' => 'double',
									'label' => __( 'Double', 'yith-plugin-product-purchase' ),
								),
								array(
									'value' => 'groove',
									'label' => __( 'Groove', 'yith-plugin-product-purchase' ),
								),
								array(
									'value' => 'hidden',
									'label' => __( 'Hidden', 'yith-plugin-product-purchase' ),
								),
								array(
									'value' => 'inherit',
									'label' => __( 'Inherit', 'yith-plugin-product-purchase' ),
								),
								array(
									'value' => 'initial',
									'label' => __( 'Initial', 'yith-plugin-product-purchase' ),
								),
								array(
									'value' => 'outset',
									'label' => __( 'Outset', 'yith-plugin-product-purchase' ),
								),
								array(
									'value' => 'revert',
									'label' => __( 'Revert', 'yith-plugin-product-purchase' ),
								),
								array(
									'value' => 'ridge',
									'label' => __( 'Ridge', 'yith-plugin-product-purchase' ),
								),
							),
						),
						array(
							'type'      => 'colorpicker',
							'label'     => __( 'color', 'yith-plugin-product-purchase' ),
							'id'        => '_yith-pp-set-border-color',
							'name'      => '_yith_pp_set_border_color',
							'value'     => get_option( '_yith_pp_set_border_color', '#d8d8d8' ),
							'container' => '_yith-setting-option',
							'enable'    => 'yes',
							'adm_pro'   => 'no',
						),
						array(
							'type'      => 'number',
							'label'     => __( 'radius', 'yith-plugin-product-purchase' ),
							'id'        => '_yith-pp-set-border-radius',
							'name'      => '_yith_pp_set_border_radius',
							'value'     => intval( get_option( '_yith_pp_set_border_radius', 7 ) ),
							'container' => '_yith-setting-option',
							'enable'    => 'yes',
							'adm_pro'   => 'no',
						),

					),
				),
				array(
					'title'        => __( 'Badge Position', 'yith-plugin-product-purchase' ),
					'id'           => 'yith-pp-bdg-pos',
					'section_name' => 'options_bdg_pos',
					'container'    => '_yith-setting-option',
					'fields'       => array(
						array(
							'type'      => 'radio',
							'label'     => __( 'In Shop', 'yith-plugin-product-purchase' ),
							'id'        => '_yith-pp-bdg-pos-shop',
							'class'     => '_yith-pp-bdg-pos',
							'name'      => '_yith_pp_bdg_pos_shop',
							'enable'    => 'yes',
							'value'     => get_option( '_yith_pp_bdg_pos_shop', 'top_right' ),
							'container' => '',
							'options'   => array(
								array(
									'value' => 'top_right',
									'text'  => __( 'Top Right', 'yith-plugin-product-purchase' ),
									'id'    => '_yith-pp-bdg-pos-shop-tr',
								),
								array(
									'value' => 'top_left',
									'text'  => __( 'Top Left', 'yith-plugin-product-purchase' ),
									'id'    => '_yith-pp-bdg-pos-shop-tl',
								),
							),
						),
						array(
							'type'      => 'radio',
							'label'     => __( 'In Product', 'yith-plugin-product-purchase' ),
							'id'        => '_yith-pp-bdg-pos-prod',
							'class'     => '_yith-pp-bdg-pos',
							'name'      => '_yith_pp_bdg_pos_prod',
							'value'     => get_option( '_yith_pp_bdg_pos_prod', 'top_right' ),
							'container' => '',
							'enable'    => 'yes',
							'options'   => array(
								array(
									'text'  => __( 'Top Right', 'yith-plugin-product-purchase' ),
									'id'    => '_yith-pp-bdg-pos-prod-tr',
									'value' => 'top_right',
								),
								array(
									'text'  => __( 'Top Left', 'yith-plugin-product-purchase' ),
									'id'    => '_yith-pp-bdg-pos-prod-tl',
									'value' => 'top_left',
								),
							),
						),
					),
				),
			);

			foreach ( $setting_fields as $sf ) {
				add_settings_section(
					$sf['id'],
					$sf['title'],
					'',
					$page_name,
				);

				foreach ( $sf['fields'] as $fields ) {
					add_settings_field(
						$fields['id'],
						'',
						array( $this, 'wc_pp_show_settings' ),
						$page_name,
						$sf['id'],
						$fields,
					);
					register_setting( $page_name, $fields['name'] );
				}
			}
		}	
		/**
		 * Register Settings
		 *
		 * @param  mixed $field_data
		 * @param  mixed $index
		 * @return void
		 */
		public function wc_pp_show_settings( $field_data, $index = null ) {
			yith_pp_get_view( '/input-fields/' . $field_data['type'] . '.php', $field_data, $index );
		}
	}
}
