<?php //phpcs:ignore
/**
 * This file belongs to the YITH PP Plugin Product Purchase Note.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

if ( ! defined( 'YITH_PP_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PP_AJAX' ) ) {
	/**
	 * YITH_PP_AJAX
	 */
	class YITH_PP_AJAX {
		/**
		 * Main Instance
		 *
		 * @var YITH_PP_AJAX
		 * @since 1.0.0
		 * @access private
		 */
		private static $instance;
		/**
		 * Main plugin Instance
		 * @return YITH_PP_AJAX Main instance
		 * @author Angel Gallozo <luisangelgallozo@gmail.com>
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}
		/**
		 * YITH_PP_AJAX constructor.
		 */
		private function __construct() {
			// Increment Price in Product Page.
			add_action( 'wp_ajax_increment_price', array( $this, 'pp_increment_price' ) );
			add_action( 'wp_ajax_no_priv_increment_price', array( $this, 'pp_increment_price' ) );
		}
		/**
		 * Increment Price
		 *
		 * @return void
		 */
		public function pp_increment_price() {
			$product      = wc_get_product( $_POST['post_id'] );
			$option_price = $product->get_meta( '_yith_pp_price_settings' );
			$price        = $product->get_meta( '_yith_pp_price' );
			$mensaje      = '+ 0€';
			if ( 'free' !== $option_price && isset( $price ) &&  (0 < $_POST['msj'] ) ) {
				$chars_free  = intval( $product->get_meta( '_yith_pp_free_char' ) );
				$total_price = 0;
				if ( $_POST['msj'] > $chars_free ) {
					switch ( $option_price ) {
						case 'fixed':
							$total_price = $price;
							break;
						case 'price-per-char':
							$total_price = ( $_POST['msj'] - $chars_free ) * $price;
							break;
					}
				}
				$mensaje = '+ ' . $total_price . '€';
			}
			wp_send_json( array( 'message' => $mensaje ) );
		}
	}
}
