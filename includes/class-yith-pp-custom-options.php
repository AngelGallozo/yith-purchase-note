<?php //phpcs:ignore
/**
 * This file belongs to the YITH PP Plugin Product Purchase Note.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

if ( ! defined( 'YITH_PP_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_PP_CO' ) ) {
	/**
	 * YITH_PP_CO
	 */
	class YITH_PP_CO {
		/**
		 * Main Instance
		 *
		 * @var YITH_PP_CO
		 * @since 1.0.0
		 * @access private
		 */
		private static $instance;
		/**
		 * Main plugin Instance
		 * @return YITH_PP_CO Main instance
		 * @author Angel Gallozo <luisangelgallozo@gmail.com>
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}
		/**
		 * YITH_PP_CO constructor.
		 */
		private function __construct() {
			add_filter( 'woocommerce_product_data_tabs', array( $this, 'wc_pp_add_options' ) );
			add_action( 'woocommerce_product_data_panels', array( $this, 'wc_pp_data_tab' ) );
			add_action( 'woocommerce_process_product_meta', array( $this, 'wc_pp_save_options' ) );
		}
		/**
		 * Add Section to the Options
		 *
		 * @return void
		 */
		public function wc_pp_add_options( $tabs ) {
			$tabs['options_pp'] = array(
				'label'    => __( 'Product Purchase Options', 'yith-plugin-product-purchase' ),
				'target'   => 'wc_pp_data_tab',
				'class'    => array(),
				'priority' => 60,
			);
			return $tabs;
		}
		/**
		 * Define Fields
		 *
		 * @return void
		 */
		public function wc_pp_data_tab() {
			global $post;
			$enable_pp = get_post_meta( $post->ID, '_yith_pp_enable', true );
			$options = array(
				array(
					'type'      => 'toggle',
					'label'     => __( 'Enabled', 'yith-plugin-product-purchase' ),
					'id'        => '_yith-pp-enable',
					'name'      => '_yith_pp-enable',
					'value'     => ( ( '' !== $enable_pp ) ? $enable_pp : 'no' ),
					'enable'    => 'yes',
					'container' => '_yith-pp-cont-pp-toggle',
				),
				array(
					'type'      => 'text',
					'label'     => __( 'Note Label', 'yith-plugin-product-purchase' ),
					'id'        => '_yith-pp-note-label',
					'name'      => '_yith_pp_note_label',
					'size'      => 40,
					'value'     => ( ( '' !== get_post_meta( $post->ID, '_yith_pp_note_label', true ) ) ? get_post_meta( $post->ID, '_yith_pp_note_label', true ) : 'Note' ),
					'enable'    => 'no',
					'container' => '_yith-pp-cont-pp-text',
				),
				array(
					'type'      => 'textarea',
					'label'     => __( 'Note Description', 'yith-plugin-product-purchase' ),
					'id'        => '_yith-pp-note-desc',
					'name'      => '_yith_pp_note_desc',
					'value'     => ( ( '' !== get_post_meta( $post->ID, '_yith_pp_note_desc', true ) ) ? get_post_meta( $post->ID, '_yith_pp_note_desc', true ) : '' ),
					'enable'    => 'no',
					'container' => '_yith-pp-cont-pp-text-area',
				),
				array(
					'type'      => 'radio',
					'label'     => __( 'Field type', 'yith-plugin-product-purchase' ),
					'id'        => '_yith-pp-field-type',
					'class'     => '',
					'name'      => '_yith_pp_field_type',
					'value'     => ( ( '' !== get_post_meta( $post->ID, '_yith_pp_field_type', true ) ) ? get_post_meta( $post->ID, '_yith_pp_field_type', true ) : 'text' ),
					'enable'    => 'no',
					'container' => 'field _yith-pp-cont-pp-radio',
					'options' => array(
						array(
							'text'  => __( 'Text', 'yith-plugin-product-purchase' ),
							'id'    => '_yith-pp-field-type-text',
							'value' => 'text',
						),
						array(
							'value' => 'textarea',
							'text'  => __( 'Text Area', 'yith-plugin-product-purchase' ),
							'id'    => '_yith-pp-field-type-text-area',
						),
					),
				),
				array(
					'type'      => 'radio',
					'label'     => __( 'Price settings', 'yith-plugin-product-purchase' ),
					'id'        => '_yith-pp-price-settings',
					'class'     => '',
					'name'      => '_yith_pp_price_settings',
					'value'     => ( ( '' !== get_post_meta( $post->ID, '_yith_pp_price_settings', true ) ) ? get_post_meta( $post->ID, '_yith_pp_price_settings', true ) : 'free' ),
					'enable'    => 'no',
					'container' => 'field _yith-pp-cont-pp-radio',
					'options'   => array(
						array(
							'text'  => __( 'Free', 'yith-plugin-product-purchase' ),
							'id'    => 'option-free',
							'value' => 'free',
						),
						array(
							'text'  => __( 'Fixed', 'yith-plugin-product-purchase' ),
							'id'    => 'option-fixed',
							'value' => 'fixed',
						),
						array(
							'text'  => __( 'Price per character', 'yith-plugin-product-purchase' ),
							'id'    => 'option-ppp',
							'value' => 'price-per-char',
						),
					),
				),
				array(
					'type'      => 'text',
					'label'     => __( 'Price', 'yith-plugin-product-purchase' ),
					'id'        => '_yith-pp-price',
					'name'      => '_yith_pp_price',
					'adm_pro'   => 'yes',
					'enable'    => 'no',
					'value'     => ( ( '' !== get_post_meta( $post->ID, '_yith_pp_price', true ) ) ? get_post_meta( $post->ID, '_yith_pp_price', true ) : 0 ),
					'container' => '_yith-pp-cont-pp-number',
				),
				array(
					'type'      => 'number',
					'label'     => __( 'Free characters', 'yith-plugin-product-purchase' ),
					'id'        => '_yith-pp-free-char',
					'name'      => '_yith_pp_free_char',
					'adm_pro'   => 'yes',
					'min'       => 0,
					'max'       => 100,
					'enable'    => 'no',
					'value'     => ( ( '' !== get_post_meta( $post->ID, '_yith_pp_free_char', true ) ) ? get_post_meta( $post->ID, '_yith_pp_free_char', true ) : 0 ),
					'container' => '_yith-pp-cont-pp-number',
				),
				array(
					'type'      => 'toggle',
					'label'     => __( 'Show Badge', 'yith-plugin-product-purchase' ),
					'id'        => '_yith-pp-show-badge',
					'name'      => '_yith_pp_show_badge',
					'value'     => ( ( '' !== get_post_meta( $post->ID, '_yith_pp_show_badge', true ) ) ? get_post_meta( $post->ID, '_yith_pp_show_badge', true ) : 'no' ),
					'enable'    => 'no',
					'container' => '_yith-pp-cont-pp-toggle',
				),
				array(
					'type'      => 'text',
					'label'     => __( 'Badge text', 'yith-plugin-product-purchase' ),
					'id'        => 'yith-pp-badge-text',
					'name'      => '_yith_pp_badge_text',
					'size'      => 30,
					'enable'    => 'no',
					'value'     => ( ( '' !== get_post_meta( $post->ID, '_yith_pp_badge_text', true ) ) ? get_post_meta( $post->ID, '_yith_pp_badge_text', true ) : '' ),
					'container' => '_yith-pp-cont-pp-badge-text',
				),
				array(
					'type'      => 'colorpicker',
					'label'     => __( 'Badge background color', 'yith-plugin-product-purchase' ),
					'id'        => '_yith-pp-badge-bg-color',
					'name'      => '_yith_pp_badge_bg_color',
					'value'     => ( ( '' !== get_post_meta( $post->ID, '_yith_pp_badge_bg_color', true ) ) ? get_post_meta( $post->ID, '_yith_pp_badge_bg_color', true ) : '#ffffff' ),
					'enable'    => 'no',
					'adm_pro'   => 'yes',
					'container' => '_yith-pp-cont-pp-color-picker',
				),
				array(
					'type'      => 'colorpicker',
					'label'     => __( 'Badge text color', 'yith-plugin-product-purchase' ),
					'id'        => '_yith-pp-badge-text-color',
					'name'      => '_yith_pp_badge_text_color',
					'value'     => ( ( '' !== get_post_meta( $post->ID, '_yith_pp_badge_text_color', true ) ) ? get_post_meta( $post->ID, '_yith_pp_badge_text_color', true ) : '#ffffff' ),
					'enable'    => 'no',
					'adm_pro'   => 'yes',
					'container' => '_yith-pp-cont-pp-color-picker',
				),
			);
			echo '<div id="wc_pp_data_tab" class="panel woocommerce_options_panel hidden">';
			$this->wc_pp_show_options( $options );
			echo '</div>';
		}
		/**
		 * Show Options
		 *
		 * @param  mixed $options
		 * @return void
		 */
		public function wc_pp_show_options( $options ) {
			foreach ( $options as $field ) {
				yith_pp_get_view( '/input-fields/' . $field['type'] . '.php', $field );
			}
		}
		/**
		 * Save Options Data
		 *
		 * @param  mixed $post_id
		 * @return void
		 */
		public function wc_pp_save_options() {
			global $post;
			$product = wc_get_product( $post->ID );
			// Toggle Enable.
			if ( isset( $_POST['_yith_pp-enable'] ) && ( "yes" === $_POST['_yith_pp-enable'] ) ) {
				$yith_pp_toggle = $_POST['_yith_pp-enable'];
				$product->update_meta_data( '_yith_pp_enable', sanitize_text_field( $yith_pp_toggle ) );
				// Note Label.
				$yith_pp_note_label = isset( $_POST['_yith_pp_note_label'] ) ? $_POST['_yith_pp_note_label'] : '';
				$product->update_meta_data( '_yith_pp_note_label', sanitize_text_field( $yith_pp_note_label ) );
				// Note Description.
				$yith_pp_note_desc = isset( $_POST['_yith_pp_note_desc'] ) ? $_POST['_yith_pp_note_desc'] : '';
				$product->update_meta_data( '_yith_pp_note_desc', sanitize_text_field( $yith_pp_note_desc ) );
				// Field type.
				$yith_pp_field_type = isset( $_POST['_yith_pp_field_type'] ) ? $_POST['_yith_pp_field_type'] : 'text';
				$product->update_meta_data( '_yith_pp_field_type', sanitize_text_field( $yith_pp_field_type ) );
				// Price settings.
				if ( isset( $_POST['_yith_pp_price_settings'] )){ 
					if ( 'free' !== $_POST['_yith_pp_price_settings'] ) {
						// Free characters.
						$yith_pp_free_charact = isset( $_POST['_yith_pp_free_char'] ) ? $_POST['_yith_pp_free_char'] : '';
						$product->update_meta_data( '_yith_pp_free_char', sanitize_text_field( $yith_pp_free_charact ) );
						// Price.
						$yith_pp_price = ( isset( $_POST['_yith_pp_price'] ) && is_numeric( $_POST['_yith_pp_price'] ) ) ? $_POST['_yith_pp_price'] : 0;
						$product->update_meta_data( '_yith_pp_price', sanitize_text_field( $yith_pp_price ) );
					}
					$yith_pp_price_settings = isset( $_POST['_yith_pp_price_settings'] ) ? $_POST['_yith_pp_price_settings'] : '';
					$product->update_meta_data( '_yith_pp_price_settings', sanitize_text_field( $yith_pp_price_settings ) );
				}
				// Badge Toggle.
				if ( isset( $_POST['_yith_pp_show_badge'] ) && ( "yes" === $_POST['_yith_pp_show_badge']) ) {
					$yith_pp_badge_toggle = $_POST['_yith_pp_show_badge'];
					$product->update_meta_data( '_yith_pp_show_badge', sanitize_text_field( $yith_pp_badge_toggle ) );
					// Badge text.
					$yith_pp_badge_text = isset( $_POST['_yith_pp_badge_text'] ) ? $_POST['_yith_pp_badge_text'] : '';
					$product->update_meta_data( '_yith_pp_badge_text', sanitize_text_field( $yith_pp_badge_text ) );
					// Badge background color.
					$yith_pp_badge_bg_color = isset( $_POST['_yith_pp_badge_bg_color'] ) ? $_POST['_yith_pp_badge_bg_color'] : '';
					$product->update_meta_data( '_yith_pp_badge_bg_color', sanitize_text_field( $yith_pp_badge_bg_color ) );
					// Badge text color.
					$yith_pp_badge_text_color = isset( $_POST['_yith_pp_badge_text_color'] ) ? $_POST['_yith_pp_badge_text_color'] : '';
					$product->update_meta_data( '_yith_pp_badge_text_color', sanitize_text_field( $yith_pp_badge_text_color ) );	
				} else {
					$product->update_meta_data( '_yith_pp_show_badge', 'no' );
				}
				// Save Data.
			} else {
				$product->update_meta_data( '_yith_pp_enable', 'no' );
				$product->update_meta_data( '_yith_pp_show_badge', 'no' );
			}
			$product->save();
		}
	}
}
