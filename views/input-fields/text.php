<?php
?>
<div class="<?php echo esc_attr( $container ); ?>" <?php echo ( 'no' === $enable ) ? 'hidden' : ''; ?>> 
<p class="field">
<?php if ( isset( $label ) ) { ?>
    <label for="<?php echo esc_attr( $id ); ?>">
        <?php esc_html_e( $label, 'yith-personalize-products' ); ?>
    </label>
<?php } ?>
<?php if ( isset( $desc ) ) { ?>
    <p>
    <?php echo $desc ?>
    </p>
<?php } ?>
    <input type="text" id="<?php echo isset( $id ) ? esc_attr( $id ) : ''; ?>"
        name="<?php echo isset( $name ) ? esc_attr( $name ) : ''; ?>"
        value="<?php echo isset( $value ) ? esc_html( $value ) : ''; ?>"
        size="<?php echo isset( $size ) ? esc_html( $size ) : 10; ?>"
        data-post_id = "<?php echo ( isset( $post_id ) ) ? esc_html( $post_id ) : ''; ?>">
</p>
</div>
