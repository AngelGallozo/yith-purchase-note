<div class="<?php echo esc_attr( $container );?> " <?php echo ( 'no' === $enable ) ? 'hidden' : ''; ?>>
<?php if ( 'yes' === $adm_pro){ ?>
<p class="field">
<?php } ?>

<?php if ( isset( $label ) ) { ?>
    <label for="<?php echo esc_attr( $id ); ?>" >
        <?php esc_html_e( $label, 'yith-personalize-products' ); ?>
    </label>
<?php } ?>
    <input type="text" id="<?php echo esc_attr( $id ); ?>" name ="<?php esc_html_e( $name, 'yith-personalize-products' );?>" value ="<?php esc_html_e( $value, 'yith-personalize-products' );?>" class="color-picker"/>    
<?php if ( 'yes' === $adm_pro){ ?>
    </p>
<?php } ?>
</div>
