<div class="<?php echo esc_attr( $container ); ?>" <?php echo ( 'no' === $enable ) ? 'hidden' : ''; ?>>
<?php if ( 'yes' === $adm_pro){ ?>
<p class="field">
<?php } ?>
<?php if ( isset( $label ) ) { ?>
    <label for="<?php echo esc_attr( $id ); ?>" >
        <?php esc_html_e( $label, 'yith-personalize-products' ); ?>
    </label>
<?php } ?>

    <input type="number" id="<?php echo isset( $id ) ? esc_attr( $id ) : ''; ?>"
        name="<?php echo isset( $name ) ? esc_attr( $name ) : ''; ?>"
        min="<?php echo isset( $min ) ? esc_attr( $min ) : ''; ?>"
        max="<?php echo isset( $max ) ? esc_attr( $max ) : ''; ?>"
        value="<?php echo isset( $value ) ? esc_html( $value ) : 0; ?>">
<?php if ( 'yes' === $adm_pro){ ?>
</p>
<?php } ?>
</div>
