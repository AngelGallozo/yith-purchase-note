<?php
?>
<div class="<?php echo esc_attr( $container); ?>">
<?php 
if ( isset( $label ) ) { ?>
        <label for="<?php echo esc_attr( $id ); ?>-lb">
        <?php esc_html_e( $label, 'yith-personalize-products' ); ?>
        </label>
<?php } ?>

<select  id="<?php echo esc_attr( $id ); ?>" name="<?php echo esc_attr( $name ); ?>">
<?php foreach ( $options as $count ) {?>
        <option value="<?php echo esc_attr( $count['value'] ); ?>" <?php  echo ( esc_attr( $count['value'] === $value ) ) ? ' selected' : '' ?> > 
                <?php echo esc_attr( $count['label'] ); ?>
        </option>
<?php }?>
</select>
</div>