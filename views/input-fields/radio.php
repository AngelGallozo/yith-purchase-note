<div class="<?php echo esc_attr( $container ); ?>" <?php echo ( 'no' === $enable ) ? 'hidden' : ''; ?>>
<?php 
if ( isset( $label ) ) { ?>
        <label for="<?php echo esc_attr( $id ); ?>" class="<?php echo esc_attr( $class ); ?>">
        <?php esc_html_e( $label, 'yith-personalize-products' ); ?>
        </label>
<?php } 

foreach ( $options as $count ) {?>
        <div>
        <input class="radio-item_input" name="<?php echo isset( $name ) ? esc_attr( $name ) : '';  ?>" type="radio" id="<?php echo isset( $count['id'] ) ? esc_attr( $count['id'] ) : ''; ?>"
        value="<?php echo isset( $count['value'] ) ? esc_attr( $count['value'] ) : ''; ?>"
        <?php  echo ( $count['value'] === $value ) ? 'checked' : ''; ?> >
        <label class="radio-item_lb" for="<?php echo isset( $value ) ? esc_attr( $value ) : ''; ?>">
            <?php esc_html_e( $count['text'], 'yith-personalize-products' ); ?>
        </label>
        </div>
<?php }
?>
</div>