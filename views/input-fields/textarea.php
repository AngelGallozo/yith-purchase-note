
<?php 
?>
<div class="<?php echo esc_attr( $container ); ?>" data-post_id = "<?php echo ( isset( $post_id ) ) ? esc_html( $post_id ) : ''; ?>" <?php echo ( 'no' === $enable ) ? 'hidden' : ''; ?>>
<p class="field">
<?php
if ( isset( $label ) ) { ?>
    <label for="<?php echo esc_attr( $id ); ?>">
    <?php esc_html_e( $label, 'yith-plugin-product-purchase' ); ?>
    </label>
<?php } ?>
<?php if ( isset( $desc ) ){ ?>
        <span id="desc"> <?php echo $desc ?></span>
 <?php } ?>
<textarea type="text" id="<?php echo esc_attr( $id ); ?>" name="<?php echo esc_attr( $name ); ?>"
    data-post_id = "<?php echo ( isset( $post_id ) ) ? esc_html( $post_id ) : ''; ?>">
    <?php echo isset( $value ) ? esc_html( $value ) : ''; ?>
</textarea>
</p>

</div>

