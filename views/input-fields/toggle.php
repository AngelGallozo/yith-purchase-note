<?php

?>
<div <?php if ( '_yith-pp-enable' !== $id ) { echo ( 'no' === $enable ) ? 'hidden' : ''; echo ' class="' . esc_attr( $container ) . '"'; } ?>>
<p class="field">
    <label> <?php esc_html_e( $label, 'yith-personalize-products' ); ?>
    </label>
    <label class="switch">
      <input type="checkbox" id="<?php echo isset( $id ) ? esc_attr( $id ) : ''; ?>"
        name="<?php echo isset( $name ) ? esc_attr( $name ) : ''; ?>"
        value="<?php echo isset( $value ) ? esc_html( $value ) : 'no'; ?>" 
        <?php  echo ( 'yes' === $value ) ? 'checked' : ''; ?> >
      <span class="slider round"></span>
    </label>
    
</p>
</div>