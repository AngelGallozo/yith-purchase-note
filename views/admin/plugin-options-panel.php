<?php 

?>

<div class="wrap">
    <h1><?php esc_html_e( 'Note Box Data', 'yith-plugin-personalize-products' );?></h1>

    <form method="post" action="options.php">
        <?php
            settings_fields( 'pp-options-page' );
		    do_settings_sections( 'pp-options-page' );
            submit_button();
        ?>

    </form>
</div>