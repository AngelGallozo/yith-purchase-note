<?php

/**
 * Plugin Name: YITH Plugin Product Pruchase Note
 * Description: Products Purchase Note for YITH Plugins
 * Version: 1.0.0
 * Author: Angel Gallozo
 * Author URI: https://yithemes.com/
 * Text Domain: yith-plugin-product-purchase
 */

! defined( 'ABSPATH' ) && exit;  // check if defined ABSPATH.

/* where defined PATH for Style, Assets, Templates, Views */
if ( ! defined( 'YITH_PP_VERSION' ) ) {
	define( 'YITH_PP_VERSION', '1.0.0' );
}

if ( ! defined( 'YITH_PP_DIR_URL' ) ) {
	define( 'YITH_PP_DIR_URL', plugin_dir_url( __FILE__ ) );
}

if ( ! defined( 'YITH_PP_DIR_PATH' ) ) {
	define( 'YITH_PP_DIR_PATH', plugin_dir_path( __FILE__ ) );
}

if ( ! defined( 'YITH_PP_DIR_ASSETS_URL' ) ) {
	define( 'YITH_PP_DIR_ASSETS_URL', YITH_PP_DIR_URL . 'assets' );
}

if ( ! defined( 'YITH_PP_DIR_ASSETS_CSS_URL' ) ) {
	define( 'YITH_PP_DIR_ASSETS_CSS_URL', YITH_PP_DIR_ASSETS_URL . '/css' );
}

if ( ! defined( 'YITH_PP_DIR_ASSETS_JS_URL' ) ) {
	define( 'YITH_PP_DIR_ASSETS_JS_URL', YITH_PP_DIR_ASSETS_URL . '/js' );
}

if ( ! defined( 'YITH_PP_DIR_INCLUDES_PATH' ) ) {
	define( 'YITH_PP_DIR_INCLUDES_PATH', YITH_PP_DIR_PATH . '/includes' );
}

if ( ! defined( 'YITH_PP_DIR_TEMPLATES_PATH' ) ) {
	define( 'YITH_PP_DIR_TEMPLATES_PATH', YITH_PP_DIR_PATH . '/templates' );
}


if ( ! defined( 'YITH_PP_DIR_VIEWS_PATH' ) ) {
	define( 'YITH_PP_DIR_VIEWS_PATH', YITH_PP_DIR_PATH . 'views' );
}

// Different way to declare a constant.

! defined( 'YITH_PP_INIT' ) && define( 'YITH_PP_INIT', plugin_basename( __FILE__ ) );
! defined( 'YITH_PP_SLUG' ) && define( 'YITH_PP_SLUG', 'yith-plugin-product-purchase' );
! defined( 'YITH_PP_SECRETKEY' ) && define( 'YITH_PP_SECRETKEY', 'zd9egFgFdF1D8Azh2ifA' );


if ( ! function_exists( 'yith_pp_init_classes' ) ) {
	/**
	 * Include the scripts
	 *
	 * @return void
	 */
	function yith_pp_init_classes() {

		load_plugin_textdomain( 'yith-plugin-product-purchase', false, basename( dirname( __FILE__ ) ) . '/languages/' );

		// Require all the files you include on your plugins. Example.
		require_once YITH_PP_DIR_INCLUDES_PATH . '/class-yith-pp-product-purchase.php';

		if ( class_exists( 'YITH_PP_Plugin_Product_Purchase' ) ) {
			/*
			*	Call the main function
			*/
			yith_pp_plugin_product_purchase();
		}
	}
}

add_action( 'plugins_loaded', 'yith_pp_init_classes', 11 );
